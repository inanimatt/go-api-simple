# Basic JSON API example

Run it with `go run api.go` or build an executable with `go build api.go` then run `./api`

Endpoints:

* `/assets/*` - a standard file server for any files within the `assets` folder.
* `/about` - returns a short JSON message
* `/*` - a catchall handler for, e.g. `/world`

Read the source comments to see how it works

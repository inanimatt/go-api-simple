package main

// Import built-in packages for handling json, basic output, logging and a web server
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Our default handler - we don't need one, but it demonstrates getting parts of the URL
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
}

// The prototype for our JSON output: just a string called "Text"
// The `json:"message"` annotation tells json.Marshal to rename the property in the output
type Message struct {
	Text string `json:"message"`
}

// Our "about" handler demonstrates creating a JSON response:
// Create a message then use json.Marshal() to produce a byte-array (string) response
func about(w http.ResponseWriter, r *http.Request) {
	m := Message{"Test API v0.1"}
	b, err := json.Marshal(m)

	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
}

// The main program loop attaches the handlers to routes, then starts an SSL server
func main() {

	// Default handler (NOT just root!)
	http.HandleFunc("/", handler)

	// About
	http.HandleFunc("/about", about)

	// Serve static files in ./assets
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))

	log.Println("Listening on https://localhost:8443/")
	log.Fatal(http.ListenAndServeTLS(":8443", "localhost.crt", "localhost.key", nil))
}
